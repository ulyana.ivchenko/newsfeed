<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsController@index')->name('home')->middleware('auth');

Route::resource('users','UsersController')->only('show')->middleware('auth');
Route::resource('news','NewsController')->middleware('auth');
Route::resource('news.comments', 'CommentsController')->except('index', 'show')->middleware('auth');
Route::post('rating/news/{news}', 'NewsController@rating')->name('rating')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
