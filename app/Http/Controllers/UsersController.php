<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UsersController extends Controller
{
    /**
     * @param $id
     * @return Factory|View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $user_news = $user->news;
        $total_sum = 0;
        $total_rating = 0;
        foreach($user_news as $news) {
            $scores_sum = $news->quality + $news->relevance + $news->attitude;
            $total_sum += $scores_sum;
            $total_rating = round($total_sum / 3, 1);
        }
        return view('users.show', compact('user', 'total_rating'));
    }
}
