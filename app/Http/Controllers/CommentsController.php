<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\News;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;

class CommentsController extends Controller
{
    /**
     * @param CommentRequest $request
     * @param News $news
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(CommentRequest $request, News $news)
    {
        Auth::user();

        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->news_id = $news->id;
        $comment->user_id = $request->user()->id;
        $comment->save();

        return response()->json([
            'comment' => view('comments.comment', compact('comment', 'news'))->render()
        ], 201);
    }

    /**
     * @param News $news
     * @param Comment $comment
     * @return Factory|View
     */
    public function edit(News $news, Comment $comment)
    {
        return view('comments.edit', compact('comment', 'news'));
    }

    /**
     * @param CommentRequest $request
     * @param $news
     * @param Comment $comment
     * @return RedirectResponse|Redirector
     */
    public function update(CommentRequest $request, $news, Comment $comment)
    {
        $comment->update($request->all());
        return redirect(route('news.show', compact('comment', 'news')))->with('message', 'Comment updated!');
    }

    /**
     * @param $news
     * @param $id
     * @return JsonResponse
     */
    public function destroy(News $news, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return response()->json([
            'status' => 'ok'
        ], 200);
    }
}
