<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\NewsRequest;
use App\News;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $news = News::orderBy('publication_date', 'desc')->paginate(10);

        if (!empty($request->input('category_id'))) {
            $category_id = $request->input('category_id');
            $news = News::where('category_id', $category_id)
                ->orderBy('publication_date', 'desc')
                ->paginate(10);
        }

        if (!empty($request->input('tag_id'))) {
            $tag_id = $request->input('tag_id');
            $news = News::whereHas('tags', function ($query) use ($tag_id) {
                     $query->where('tag_id', $tag_id);
                 })
                ->orderBy('publication_date', 'desc')
                ->paginate(10);
        }

        if (!empty($request->input('category_id')) && !empty($request->input('tag_id'))) {
            $category_id = $request->input('category_id');
            $tag_id = $request->input('tag_id');
            $news = News::where('category_id', $category_id)
                 ->whereHas('tags', function ($query) use ($tag_id) {
                     $query->where('tag_id', $tag_id);
                 })
                ->orderBy('publication_date', 'desc')
                ->paginate(10);
        }

        $categories = Category::all();
        $tags = Tag::all();

        return view('news.index', compact('news', 'categories', 'tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return RedirectResponse
     */
    public function store(NewsRequest $request)
    {
        Auth::user();
        $news = new News();
        $data = $request->all();
        $news->user_id = $request->user()->id;
        $data['user_id'] = $news->user_id;
        $news->create($data);

        return redirect()->route('news.index')->with('message', 'News created! Please wait until published by admin!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        $news = News::findOrFail($id);
        $tags = $news->tags()->pluck('title')->toArray();
        $user = $news->user->id;
        return view('news.show', compact('news', 'tags', 'user'));
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        return view('news.edit', compact('news'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(NewsRequest $request, $id)
    {
        $news = News::findOrFail($id);
        $data = $request->all();
        $news->update($data);
        return redirect()->route('news.index')->with('message', 'News updated!');
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return redirect()->route('news.index')->with('message', 'News deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function rating(Request $request, $id)
    {

        $news = News::findOrFail($id);
        if ($request->input('quality') == 1) {
            $news->quality += 1;
        } else {
            $news->quality += -1;
        }

        if ($request->input('relevance') == 1) {
            $news->relevance += 1;
        } else {
            $news->relevance += -1;
        }

        if ($request->input('attitude') == 1) {
            $news->attitude += 1;
        } else {
            $news->attitude += -1;
        }

        $news->save();
        return redirect()->back();
    }
}
