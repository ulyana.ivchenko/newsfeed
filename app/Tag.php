<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    protected $fillable = ['title'];

    /**
     * @return BelongsToMany
     */
    public function news()
    {
        return $this->belongsToMany(News::class,'news_tag', 'news_id', 'tag_id');
    }
}
