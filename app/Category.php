<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    protected $fillable = ['title'];

    /**
     * @return HasMany
     */
    public function news() {
        return $this->hasMany(News::class);
    }
}
