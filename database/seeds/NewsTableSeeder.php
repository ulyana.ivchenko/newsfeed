<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = factory(\App\News::class, 30)->create();

        foreach($news as $one_news) {
            $one_news->tags()->attach(range(rand(1, 10), rand(1, 10)));
        }
    }
}
