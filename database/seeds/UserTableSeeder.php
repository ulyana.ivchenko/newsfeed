<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin.admin@gmail.com',
            'password' => Hash::make('password'),
            'is_admin' => 1,
            'created_at' => '2020-10-15 14:00:00'
        ]);

        factory(\App\User::class, 20)->create();
    }
}
