<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News;
use Faker\Generator as Faker;

$factory->define(\App\News::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 21),
        'category_id' => rand(1, 10),
        'body' => $faker->text($maxNbChars = 500),
        'publication_date' => $faker->dateTimeBetween('2020-10-01', \Carbon\Carbon::now()),
        'quality' => rand(-20, 20),
        'relevance' => rand(-20, 20),
        'attitude' => rand(-20, 20),
    ];
});
