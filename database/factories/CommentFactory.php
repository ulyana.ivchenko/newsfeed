<?php

/** @var Factory $factory */

use App\Comment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(\App\Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text($maxNbChars = 300),
        'user_id' => rand(1, 21),
        'news_id' => rand(1, 30)
    ];
});
