login админа - admin.admin@gmail.com
пароль админа - password

#1 Авторизация и регистрация пользователей

Сделано полностью, использованы встроенный функционал ларавеля.

#2 Возможность публикации новостей как администратором, так и пользователями.

Сделано полностью, если зайти на главную страницу как обычный пользователь, ссылка "Add news доступна",
если зайти как администратор - тоже.

#3 Новости должны иметь тело в виде текста, тэги, категорию, иметь дату публикации и дату создания.

Сделано полностью, если перейти по ссылке Read more на страницу отдельной новости, все указанные
параметры отображаются.

#4 Рядовой пользователь может увидеть новость на сайте, если только ей указана дата публикации и дата публикации уже наступила.
Рядовой пользователь не должен иметь возможности указать дату публикации.

Сделано почти полностью, если зайти как обычный пользователь и создать новость, то она в списке новостей не отобразится,
появится сообщение, что новость создана и что нужно подождать, пока ее опубликует администратор.
Если зайти как администратор, то новость будет доступна в конце таблицы.
При создании новости инпута для добавления даты публикации нет. Это недочет, так как надо было оставить для администратора.

#5 Администратор системы должен иметь возможность модерировать и назначать дату публикации новостям.

Сделано полностью, если под администратором зайти на сайт, то на странице просмотра отдельной новости
можно увидеть кнопки "Edit" и "Delete". Если нажать на "Delete", новость будет удалена и произойдет
редирект на главную страницу, если нажать на "Edit", администратор будет перенаправлен на страницу
редактирования новости. Там можно новость отредактировать и указать дату публикации темы.
Если назначить дату публикации и отправить форму, произойдет редирект на главную страницу.
Если снова зайти как обычный пользователь, то данная новость будет отображаться в списке.

#6 Нужно сделать функционал просмотра новостей списком с постраничностью и фильтрацией по категориям и тегам.
Также должна быть страница детального просмотра новости

Сделано полностью, пагинация по 10 страниц, есть два фильтра - по категориям и по тэгам. Можно отфильтровать только
по категории или только по тэгу, а также и по категории, и по тэгу.
Переход на страницу детального просмотра описан выше (нажать на ссылку Read more)

#7 Должен быть функционал комментирования новостей и модерирования комментариев администратором

Сделано полностью. Если перейти на страницу отдельной новости, там отобразятся комменты. Пользователь не может
редактировать или удалять чужие комменты, администратор может.

#8 Каждая новость пользователем может быть оценена по трем критериям

Сделано полностью. Есть три селекта, где можно оценить новость по трем критериям или по одному. Также указаны
данные по качеству, актуальности и отношению пользователей по каждой новости.

#9-10 У всех, кто публикует новости на сайте, должен быть рейтинг рассчитываемый по формуле:  (Ar + Q + A) / 3.
Должна быть страница профайла пользователя. На странице должна быть информация о рейтинге пользователя и его
регистрационные данные (кроме пароля, конечно).

Сделано полностью. Если нажать на имя автора статьи, произойдет редирект на страницу данного пользователя, где будут
показаны его регистрационные данные и рейтинг.

Есть недочеты - нельзя со страницы отдельного пользователя перейти по тэгам, чтобы посмотреть новости только с этим тэгом,
но это в задании прямо не указывалось.


