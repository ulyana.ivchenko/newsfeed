$(document).ready(function () {
    $('#add-comment-btn').click(function (event) {
        event.preventDefault();
        const data = $('#add-comment').serialize();
        const newsId = $('#news_id').val();
        $.ajax({
            url: `/news/${newsId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (data) {
                renderData(data.comment);
            })
            .fail(function (response) {
                let errors = response.responseJSON.errors;
                renderErrors(errors);
            });
    });

    $('.delete').on('click', function (event) {
            const commentId = $(this).attr('data-comment-id');
            const newsId = $('#news-id').val();
            const token = $('input[type=hidden]').val();
            const comment = $('#delete-comment-' + commentId);

            $.ajax({
                url: `/news/${newsId}/comments/${commentId}`,
                method: "DELETE",
                data: {_token: token}
            })
                .done(function (response) {
                    $(comment).remove();
                })
        }
    )
});

function clearForm() {
    $('#add-comment').trigger('reset');
}

function renderData(html) {
    let commentsBlock = $('#comments-block');
    $(commentsBlock).prepend(html);
    clearForm();
}

function renderErrors(errors) {
    let keys = Object.keys(errors);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        let element = $(`#${key}`);
        $(element).addClass('is-invalid');

        let html = `<div class="errors-${key}">`;
        for (let j = 0; j < errors[key].length; j++) {
            html += `<p>*${errors[key][j]}</p>`;
        }
        html += '</div>';
        $(element).parent().append(html);
    }

    $('#body').on('change', function (event) {
        if ($(this).hasClass('is-invalid')) {
            $('.errors-body').remove();
            $('#body').removeClass('is-invalid');
        }
    });
}
