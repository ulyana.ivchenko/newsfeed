@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-10">
            <h5>Edit comment:</h5>
            <form class="mt-3" method="post" action="{{route('news.comments.update', ['comment' => $comment, 'news' => $news])}}">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <textarea rows="5" class="form-control @error('body') is-invalid @enderror" id="body"
                                  name="body">{{$comment->body}}</textarea>
                        @error('body')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Edit</button>
                <a href="{{route('news.show', ['news' => $news])}}">Back</a>
            </form>
        </div>
    </div>
@endsection
