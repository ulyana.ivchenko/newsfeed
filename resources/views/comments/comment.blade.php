<div class="media" id="delete-comment-{{$comment->id}}">
    <img class="d-flex rounded-circle mt-0 mr-3" style="width: 50px; height: 50px"
         src="{{asset('/images/default_photo.jpeg')}}" alt="Image">
    <div class="media-body">
        <div class="mb-3">
            @if($comment->user_id == Auth::user()->id || Auth::user()->is_admin)
                <input type="hidden" id="news-id" value="{{$news->id}}">
                <span data-comment-id="{{$comment->id}}" class="delete" aria-hidden="true"
                      style="float: right; font-size: 20px; color:red; cursor:pointer;"
                      value="{{$news->id}}">&times;</span>
            @endif
        </div>
        <p class="pr-4">{{$comment->body}}</p>
        <div class="comment">
            <span>Commented: {{$comment->user->name}}, </span>
            <span>{{$comment->created_at->diffForHumans()}}</span>
        </div>
        <div>
            @if($comment->user_id == Auth::user()->id || Auth::user()->is_admin)
                <a href="{{route('news.comments.edit', ['news' => $news, 'comment' => $comment])}}"
                   style="font-size: 12px">Edit</a>
            @endif
        </div>
        <hr>
    </div>
</div>
