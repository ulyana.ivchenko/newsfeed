@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h5 class="mr-4">Add news</h5>
            >>
            <a class="ml-4" href="{{route('news.index')}}">Main Page</a>
        </div>

        <form method="post" action="{{route('news.store')}}">
            @csrf
            <div class="form-row mt-4">
                    <label for="category"><b>Categories</b></label><span class="text-danger">*</span>
                    <select class="custom-select @error('category_id') is-invalid @enderror" name="category_id"
                            id="category">
                        @foreach($categories as $category)
                            <option hidden disabled selected value="">Please select a category</option>
                            <option value="{{$category->id}}">
                                {{$category->title}}
                            </option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
            </div>
            <div class="form-row mt-4">
                    <label for="tags"><b>Tags</b></label><span class="text-danger">*</span>
                    <div class="form-check form-check-inline ml-2 mb-0">
                        @foreach($tags as $tag)
                            <input class="form-check-input ml-3 mr-3" type="checkbox" name="tags[]"
                                   value="{{$tag->id}}">{{$tag->title}}
                        @endforeach
                    </div>
            </div>
            <div class="form-row mt-4">
                    <label for="body"><b>Text</b></label><span class="text-danger">*</span>
                    <textarea rows="10" class="form-control @error('body') is-invalid @enderror" id="body"
                              name="body"></textarea>
                    @error('body')
                    <p class="error">{{ $message }}</p>
                    @enderror
            </div>
            <button type="submit" class="btn btn-primary mt-4 mb-4 pr-4 pl-4">Add</button>
        </form>
    </div>
@endsection
