@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="col-6">
            <h5>Edit:</h5>
            <form method="post" action="{{route('news.update', ['news' => $news])}}">
                @csrf
                @method('put')
                <div class="form-row mt-3">
                    <div class="form-group col-md-12">
                        <input class="form-control" type="date" id="publication_date"
                               name="publication_date" value="{{$news->publication_date}}">
                    </div>
                    <div class="form-group col-md-12">
                        <textarea rows="10" class="form-control @error('body') is-invalid @enderror"
                                  id="body"
                                  name="body">{{$news->body}}</textarea>
                        @error('body')
                        <p class="error">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary active pl-4 pr-4 mr-3">Edit</button>
                <a href="{{route('news.index')}}">Back</a>
            </form>
        </div>
    </div>
@endsection
