@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p style="font-size: 14px; font-weight: bold">Author: <a
                        href="{{route('users.show', ['user' => $user])}}" style="font-weight: normal">
                        {{$news->user->name}}
                    </a>
                </p>
                <p style="font-size: 14px; font-weight: bold">Created: <span
                        style="font-weight: normal">{{$news->created_at->diffForHumans()}}</span></p>
                <p style="font-size: 14px; font-weight: bold">Published: <span
                        style="font-weight: normal">{{$news->publication_date}}</span></p>
                <p style="font-size: 14px; font-weight: bold">Category: <span
                        style="font-weight: normal">{{$news->category->title}}</span></p>
                <div class="row mt-4">
                    <div class="col-md-3">
                        <p style="color: indianred">Quality score: <span
                                style="font-weight: bold">{{$news->quality}}</span></p>
                    </div>
                    <div class="col-md-4">
                        <p style="color: forestgreen">Relevance score: <span
                                style="font-weight: bold">{{$news->relevance}}</span></p>
                    </div>
                    <div class="col-md-4">
                        <p style="color: #117a8b">Attitude score: <span
                                style="font-weight: bold">{{$news->attitude}}</span></p>
                    </div>
                </div>
                <p class="mb-2 mt-4" style="text-align: justify; font-size: 15px;">
                    {{$news->body}}
                </p>
                <form method="get" action="{{route('news.index')}}">
                    <p style="font-size: 14px; font-weight: bold" class="mt-4">
                        Tags:
                        @foreach($tags as $tag)
                            <a href="{{route('news.index')}}" class="card-link ml-2"
                               style="font-size: 14px;cursor:pointer;">{{$tag}}</a>
                        @endforeach
                    </p>
                </form>
            </div>
        </div>
        <form method="post" action="{{route('rating', ['news' => $news])}}">
            @csrf
            <div class="row mt-4">
                <div class="form-group ml-1 mb-1">
                    <select class="form-control" style="font-size: 12px" name="quality">
                        <option value="">Is this news good?</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <div class="form-group ml-4 mb-1">
                    <select class="form-control" name="relevance" style="font-size: 12px">
                        <option value="">Is this news relevant?</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <div class="form-group ml-4 mb-1">
                    <select class="form-control" name="attitude" style="font-size: 12px">
                        <option value="">Did you like this news?</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <div class="form-group ml-4 mb-1">
                    <button class="btn btn-warning" style="font-size: 12px" type="submit">Submit</button>
                </div>
            </div>
        </form>


        <div class="row d-flex justify-content-end mt-5">
            <div class="btn-group">
                @if(Auth::user()->is_admin)
                    <div>
                        <a href="{{route('news.edit', ['news' => $news])}}" type="button"
                           class="btn btn-success ml-3 pr-4 pl-4">Edit</a>
                    </div>
                    <form method="post" action="{{route('news.destroy', ['news' => $news])}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger ml-3 pr-4 pl-4 mr-0">Delete
                        </button>
                    </form>
                @endif
            </div>
        </div>


        <div class="row mt-4">
            <div id="comments-block" class="col-7">
                @csrf
                @foreach($news->comments as $comment)
                    <div class="media" id="delete-comment-{{$comment->id}}">
                        <img class="d-flex rounded-circle mt-0 mr-3" style="width: 50px; height: 50px"
                             src="{{asset('/images/default_photo.jpeg')}}" alt="Image">
                        <div class="media-body">
                            <div class="mb-3">
                                @if($comment->user_id == Auth::user()->id || Auth::user()->is_admin)
                                    <input type="hidden" id="news-id" value="{{$news->id}}">
                                    <span data-comment-id="{{$comment->id}}" class="delete" aria-hidden="true"
                                          style="float: right; font-size: 20px; color:red; cursor:pointer;"
                                          value="{{$news->id}}">&times;</span>
                                @endif
                            </div>
                            <p class="pr-4">{{$comment->body}}</p>
                            <div class="comment">
                                <span>Commented: {{$comment->user->name}}, </span>
                                <span>{{$comment->created_at->diffForHumans()}}</span>
                            </div>
                            <div>
                                @if($comment->user_id == Auth::user()->id || Auth::user()->is_admin)
                                    <a href="{{route('news.comments.edit', ['news' => $news, 'comment' => $comment])}}"
                                       style="font-size: 12px">Edit</a>
                                @endif
                            </div>
                            <hr>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-4 ml-auto mt-3">
                <div class="comment-form">
                    <form id="add-comment">
                        @csrf
                        <input type="hidden" id="news_id" value="{{$news->id}}">
                        <div class="form-group">
                            <label for="body">Comment</label>
                            <textarea name="body" class="form-control" id="body" rows="3"
                                      @error('category_id') is-invalid @enderror></textarea>
                        </div>
                        @error('body')
                        <p class="error">{{ $message }}</p>
                        @enderror
                        <button id="add-comment-btn" type="submit" class="btn btn-primary btn-block">Add comment
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
