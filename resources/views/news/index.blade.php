@extends('layouts.app')
@section('content')
    <div class="container w-75">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <div class="row pt-2 pb-4">
            <a class="mr-5" href="{{route('news.create')}}">Add news</a>
        </div>

        <div>
            <form action="{{route('news.index')}}" method="get">
                <div class="row d-flex justify-content-end mb-4">
                    <div class="form-group ml-1 mb-1">
                        <select class="form-control" name="category_id" style="font-size: 12px">
                            <option value="">Filter by category</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ml-1 mb-1">
                        <select class="form-control" name="tag_id" style="font-size: 12px">
                            <option value="">Filter by tag</option>
                            @foreach($tags as $tag)
                                <option value="{{$tag->id}}">{{$tag->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ml-1 mb-1">
                        <button class="btn btn-success" style="font-size: 12px" type="submit"><i class="fas fa-filter"></i></button>
                    </div>
                </div>
            </form>
        </div>

        @foreach($news as $one_news)
            @if(Auth::user()->is_admin == 1 || $one_news->publication_date !== null && $one_news->publication_date <= \Carbon\Carbon::now())
                <div class="card mt-2" style="width: 100%">
                    <div class="card-body pb-2">
                        <blockquote class="blockquote mb-0 pt-1 mt-2">
                            <p class="text-truncate mb-0" style="font-size: 14px;">
                                {{$one_news->body}}
                            </p>
                            <p class="d-flex justify-content-end">
                                <a href="{{route('news.show', ['news' => $one_news])}}"
                                   class="card-link mt-2" style="font-size: 12px; cursor:pointer;">Read more</a>
                            </p>
                        </blockquote>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <div style="margin-top: 50px" class="col-md-13 offset-md-5">
        {{$news->links()}}
    </div>

@endsection
