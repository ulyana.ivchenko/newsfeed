@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h5 class="mr-4">User data</h5>
            >>
            <a class="ml-4" href="{{route('news.index')}}">Main Page</a>
        </div>
        <div class="mt-4">
            <p style="font-weight: bold">Name: <span style="font-weight: normal">{{$user->name}}</span></p>
            <p style="font-weight: bold">Email: <span style="font-weight: normal">{{$user->email}}</span></p>
            <p style="font-weight: bold">Rating: <span style="font-weight: bold; color: indianred">{{$total_rating}}</span></p>
        </div>
    </div>
@endsection
